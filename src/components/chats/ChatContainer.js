import React, { Component } from 'react';
import SideBar from './SideBar';
import {
  COMMUNITY_CHAT,
  MESSAGE_RECEIVED,
  MESSAGE_SENT,
  TYPING
} from '../../Events';
import ChatHeading from './ChatHeading';
import Messages from '../messages/Messages';
import MessageInput from '../messages/MessageInput';

export default class ChatContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chats: [],
      activeChat: null,
    };
  }

  componentDidMount() {
    const { socket } = this.props;
    socket.emit(COMMUNITY_CHAT, this.resetChat);
  }

  resetChat = chat => this.addChat(chat, true);

  addChat = (chat, reset) => {
    const { socket } = this.props;
    const { chats } = this.state;
    const newChats = reset ? [chat] : [...chats, chat];
    this.setState(prevState => ({
      chats: newChats,
      activeChat: reset ? chat : prevState.activeChat
    }));
    const messageEvent = `${MESSAGE_RECEIVED}-${chat.id}`;
    const typingEvent = `${TYPING}-${chat.id}`;
    socket.on(messageEvent, this.addMessageToChat(chat.id));
    socket.on(typingEvent, this.updateTypingInChat(chat.id));
  };

  addMessageToChat = chatId => (message) => {
    const { chats } = this.state;
    const newChats = chats.map((chat) => {
      if (chat.id === chatId) {
        chat.messages.push(message);
      }
      return chat;
    });
    this.setState({ chats: newChats });
  };

  sendMessage = (chatId, message) => {
    const { socket } = this.props;
    socket.emit(MESSAGE_SENT, { chatId, message });
  };

  sendTyping = (chatId, isTyping) => {
    const { socket } = this.props;
    socket.emit(TYPING, { chatId, isTyping });
  };

  setActiveChat = (activeChat) => {
    this.setState({ activeChat });
  };

  updateTypingInChat = chatId => ({ isTyping, user }) => {
    // eslint-disable-next-line react/destructuring-assignment
    if (user !== this.props.user.name) {
      const { chats } = this.state;
      const newChats = chats.map((chat) => {
        if (chat.id === chatId) {
          if (isTyping && !chat.typingUsers.includes(user)) {
            chat.typingUsers.push(user);
          } else if (!isTyping && chat.typingUsers.includes(user)) {
            // eslint-disable-next-line no-param-reassign
            chat.typingUsers = chat.typingUsers.filter(u => u !== user);
          }
        }
        return chat;
      });
      this.setState({ chats: newChats });
    }
  };

  render() {
    const { user, logout } = this.props;
    const { chats, activeChat } = this.state;
    return (
      <div className="container">
        <SideBar
          logout={logout}
          chats={chats}
          user={user}
          activeChat={activeChat}
          setActiveChat={this.setActiveChat}
        />
        <div className="chat-room-container">
          {
            activeChat !== null ? (
              <div className="chat-room">
                <ChatHeading name={activeChat.name} />
                <Messages
                  messages={activeChat.messages}
                  user={user}
                  typingUsers={activeChat.typingUsers}
                />
                <MessageInput
                  chatName={activeChat.name}
                  sendMessage={message => this.sendMessage(activeChat.id, message)}
                  sendTyping={isTyping => this.sendTyping(activeChat.id, isTyping)}
                />
              </div>
            ) : (
              <div className="chat-room-choose">
                <h3>Choose a chat!</h3>
              </div>
            )
          }
        </div>
      </div>
    );
  }
}
