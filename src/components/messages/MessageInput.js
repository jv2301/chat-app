/* eslint-disable no-unused-expressions */
import React, { Component } from 'react';

export default class MessageInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      message: '',
      isTyping: false
    };
  }

  componentWillUnmount() {
    this.stopCheckingTyping();
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.sendMessage();
    this.setState({ message: '' });
  };

  sendMessage = () => {
    // eslint-disable-next-line react/destructuring-assignment
    this.props.sendMessage(this.state.message);
  };

  sendTyping = () => {
    this.lastUpdateTime = Date.now();
    const { isTyping } = this.state;
    if (!isTyping) {
      this.setState({ isTyping: true });
      // eslint-disable-next-line react/destructuring-assignment
      this.props.sendTyping(true);
      this.startCheckingTyping();
    }
  };

  startCheckingTyping = () => {
    this.typingInterval = setInterval(() => {
      if ((Date.now() - this.lastUpdateTime) > 300) {
        this.setState({ isTyping: false });
        this.stopCheckingTyping();
      }
    }, 300);
  };

  stopCheckingTyping = () => {
    if (this.typingInterval) {
      clearInterval(this.typingInterval);
      // eslint-disable-next-line react/destructuring-assignment
      this.props.sendTyping(false);
    }
  };

  render() {
    const { message } = this.state;
    const { chatName } = this.props;
    return (
      <div className="message-input">
        <form
          onSubmit={this.handleSubmit}
          className="message-form"
        >
          <input
            id="message"
            ref="messageinput"
            type="text"
            className="form-control"
            value={message}
            autoComplete="off"
            placeholder={`Message #${chatName}`}
            onKeyUp={(e) => { e.keyCode !== 13 && this.sendTyping(); }}
            onChange={
              ({ target }) => {
                this.setState({ message: target.value });
              }
            }
          />
          <button
            disabled={message.length < 1}
            type="submit"
            className="send"
          >
            {' '}
            Send
            {' '}
          </button>
        </form>
      </div>
    );
  }
}
