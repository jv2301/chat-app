const server = require('http').createServer();
const io = require('socket.io')(server);

module.exports.io = io;

const PORT = process.env.PORT || 3231;
const SocketManager = require('./SocketManager');

io.on('connection', SocketManager);

server.listen(PORT, () => {
  // eslint-disable-next-line no-console
  console.log(`Listening on port ${PORT}`);
});
