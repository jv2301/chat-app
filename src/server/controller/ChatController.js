const { io } = require('../index');
const { createUser, createMessage, createChat } = require('../../Factories');
const {
  MESSAGE_RECEIVED,
  TYPING,
  USER_CONNECTED,
  USER_DISCONNECTED
} = require('../../Events');

const communityChat = createChat();
let connectedUsers = {};

function ChatController(socket) {
  let sendMessageToChatFromUser;
  let sendTypingFromUser;

  const isUser = (userList, username) => username in userList;

  const addUser = (userList, user) => {
    const newList = Object.assign({}, userList);
    newList[user.name] = user;
    return newList;
  };

  const removeUser = (userList, username) => {
    const newList = Object.assign({}, userList);

    // eslint-disable-next-line no-param-reassign
    delete userList[username];
    return newList;
  };

  const sendTypingToChat = user => (chatId, isTyping) => {
    io.emit(`${TYPING}-${chatId}`, { user, isTyping });
  };

  const sendMessageToChat = sender => (chatId, message) => {
    io.emit(`${MESSAGE_RECEIVED}-${chatId}`, createMessage({ message, sender }));
  };

  const handleVerifyUser = (nickname, callback) => {
    if (isUser(connectedUsers, nickname)) {
      callback({ isUser: true, user: null });
    } else {
      callback({ isUser: false, user: createUser({ name: nickname }) });
    }
  };

  const handleUserConnected = (user) => {
    connectedUsers = addUser(connectedUsers, user);

    // eslint-disable-next-line no-param-reassign
    socket.user = user;

    sendMessageToChatFromUser = sendMessageToChat(user.name);
    sendTypingFromUser = sendTypingToChat(user.name);

    io.emit(USER_CONNECTED, connectedUsers); // broadcast
  };

  const handleUserDisconnected = () => {
    if ('user' in socket) {
      connectedUsers = removeUser(connectedUsers, socket.user.name);
      io.emit(USER_DISCONNECTED, connectedUsers);
    }
  };

  const handleLogout = () => {
    if ('user' in socket) {
      connectedUsers = removeUser(connectedUsers, socket.user.name);
      io.emit(USER_DISCONNECTED, connectedUsers);
    }
  };

  const handleCommunityChat = (callback) => {
    callback(communityChat);
  };

  const handleMessageSent = ({ chatId, message }) => {
    sendMessageToChatFromUser(chatId, message);
  };

  const handleTyping = ({ chatId, isTyping }) => {
    sendTypingFromUser(chatId, isTyping);
  };

  return {
    handleVerifyUser,
    handleUserConnected,
    handleUserDisconnected,
    handleLogout,
    handleCommunityChat,
    handleMessageSent,
    handleTyping
  };
}

module.exports = ChatController;
