const {
  COMMUNITY_CHAT,
  DISCONNECT,
  LOGOUT,
  MESSAGE_SENT,
  TYPING,
  USER_CONNECTED,
  VERIFY_USER
} = require('../Events');
const ChatController = require('./controller/ChatController');

module.exports = (socket) => {
  const chatController = ChatController(socket);

  socket.on(VERIFY_USER, chatController.handleVerifyUser);
  socket.on(USER_CONNECTED, chatController.handleUserConnected);
  socket.on(DISCONNECT, chatController.handleUserDisconnected);
  socket.on(LOGOUT, chatController.handleLogout);
  socket.on(COMMUNITY_CHAT, chatController.handleCommunityChat);
  socket.on(MESSAGE_SENT, chatController.handleMessageSent);
  socket.on(TYPING, chatController.handleTyping);
};
