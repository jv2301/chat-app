import React from 'react';
import Layout from './components/Layout';
import './index.css';

export default () => (
  <div className="container">
    <Layout title="Chat App Baby" />
  </div>
);
