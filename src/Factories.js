const uuidv4 = require('uuid/v4');

const createUser = ({ name = '' } = {}) => ({
  id: uuidv4(),
  name
});

// eslint-disable-next-line prefer-template
const getTime = date => `${date.getHours()}:${('0' + date.getMinutes()).slice(-2)}`;

const createMessage = ({ message = '', sender = '' } = {}) => ({
  id: uuidv4(),
  time: getTime(new Date(Date.now())),
  message,
  sender
});

const createChat = ({ messages = [], name = 'Community', users = [] } = {}) => ({
  id: uuidv4(),
  name,
  messages,
  users,
  typingUsers: []
});

module.exports = {
  createMessage,
  createChat,
  createUser
};
